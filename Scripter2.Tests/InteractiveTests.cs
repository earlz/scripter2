﻿using Earlz.Scripter2.Core;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scripter2.Tests
{
    [TestFixture]
    class InteractiveTests
    {
        [Test]
        public void Maintains_Variables_Across_Chunks()
        {
            var s = new ScriptingEngine();
            Assert.AreEqual(@"=> ""foo""", s.ExecuteChunk("var x='foo';"));
            Assert.AreEqual(@"=> ""foobar""", s.ExecuteChunk("x+'bar';"));
        }
    }
}
