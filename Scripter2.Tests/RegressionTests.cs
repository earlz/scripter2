﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Earlz.Scripter2.Core;


namespace Earlz.Scripter2.Tests
{
    [TestFixture]
    class RegressionTests
    {
        [Test]
        public void Implicit_Return_Values()
        {
            string s = @"
var foo=function(){ 
    var tmp='meh';
    10;
    tmp+'foo';
};
assert(foo() == 'mehfoo', 'expecting foo to equal 10');
";
            RunTest(s);
        }
        [Test]
        public void Should_Not_Need_Return()
        {
            string s = @"
var s='meh';";
            RunTest(s);
        }
        [Test]
        public void ThisValue()
        {
            string s = @"
var Counter=function(){
    var tmp=[];
    tmp['increment']=function(){ this.count++; };
    tmp['count']=0;
    return tmp;
};
var c1=Counter();
var c2=Counter();
c1.increment();

assert(c1.count==1, 'c1 should be 1');
assert(c2.count==0, 'c2 should still be 0');
c2.increment();
assert(c1.count==1, 'c1 should still be 1');
assert(c2.count==1, 'c2 should now be 1');

return;
";
            RunTest(s);
        }
        [Test]
        public void Functions()
        {
            string s=@"
var f=function(){
  return 10;
};
assert(f()==10,'f() is not equal to 10');
f=function(x){
  return x+10;
};
assert(f(5)==15, 'f(5) is not equal to 15');
var bar=function(foo){
  return foo(10);
};
assert(bar(f)==20, 'bar(f) is not equal to 20');

return;";
            RunTest(s);
        }

        [Test]
        public void Keywords()
        {
            string s = @"
var x=0;
if(x==0){
  x=1;
}else{
  x=2;
}
assert(x==1, 'if statement not correct x should be 1');
if(x==0){
  x=5;
}else{
  x=6;
}
assert(x==6, 'if statement not correct x should be 6');

if(x==6){
  x=10;
}
assert(x==10, 'if statement not correct x should be 10');
if(x==20){
  x=15;
}
assert(x==10, 'if statement not correct x should be 10 after null if');

var i=0;
while(i<1000){
  i++;
}
assert(i==1000, 'i should be 1000 after loop');

var s='';
var t=[1, 2, 3];
foreach(key => value in t){
  s=s+(key as string)+(value as string);
}
assert(s=='011223', 'foreach doesn\'t generate proper output');
var meh='';
foreach(value in t)
{
    meh=meh+value as string;
}
assert(s=='123', 'foreach should generate proper output without key => value');
return;
";
            RunTest(s);
        }

        [Test]
        public void Operations()
        {
            string s = @"
var x=10;
var y=15;

y=x++;
assert(y==10, 'y should be 10');
y=++x;
assert(y==12, 'y should be 12');

x=-10;
assert(x==0-10, 'x isn\'t -10');
return;
";
            RunTest(s);
        }

        [Test]
        public void Scope()
        {
            string s = @"

var foo=10;
var bar=function(){
  if(isdefined(foo)){
    return false;
  }else{
    return true;
  }
};
assert(bar(), 'foo should be undefined');
global var g=20;
var foobar=function(){
  if(isdefined(g)){
    return true;
  }else{
    return false;
  }
};
assert(foobar(), 'g should be defined');
var s1=10;
var s2=20;
var scope=function(s1){
  s1++;
};
scope(s2);
assert(s1==10, 'scope isn\'t correct');
assert(s2==20, 'scope isn\'t correct 2');

var s3=['foo' => 'bar'];
var scope2=function(s){
  s.foo='baz';
};
scope2(s3);
assert(s3.foo=='baz', 'tables arent passed by reference');

return;
";
            RunTest(s);
        }

        public void Tables()
        {
            string s = @"
var t=[0 => 1, 2, 'foo' => 'bar', 3];
if(t[0]!=2){
 return 'wrong1';
}
if(t[1]!=3){
  return 'wrong2';
}
if(t.foo!='bar'){
  return 'wrong3';
}

return 'correct';
";
            RunTest(s);
        }





        void RunTest(string s)
        {
            var t = new TestRunner(s);
            var result=t.Test();
            Assert.IsTrue(result.Passed, result.Message);
        }

public class TestStatus
	{
		public bool Passed;
		public string Message;
		public long TimePassed;
	}
	class AssertionFailedException: ApplicationException{
		public AssertionFailedException(string msg): base(msg){
			
		}
	}
	public class TestRunner
	{
		bool NeedsToPass=true; //some tests are designed to fail
		ScriptingEngine Engine;
		
		public TestRunner(string script)
		{
			Engine=new ScriptingEngine(script);
			var args=new VariableList();
			args.Add(new VariableContainer("condition"));
			args.Add(new VariableContainer("message"));
			Engine.Global.Add(new VariableContainer("assert", new FunctionVariable(new ExternalScriptFunction(ScriptAssert), args)));
			args = new VariableList();
			Engine.Global.Add(new VariableContainer("will_fail", new FunctionVariable(new ExternalScriptFunction(WillFail), args)));
			args=new VariableList();
			Engine.Global.Add(new VariableContainer("is_debug", new FunctionVariable(new ExternalScriptFunction(IsDebug), args)));
		}
		BaseVariable ScriptAssert(VariableList args, ScriptingEngine s){
			bool cond=args.Get<bool>(0);
			if (cond){
				return new NullVariable();
			}else{
				throw new AssertionFailedException(args.Get<string>(1));
			}
		}
		BaseVariable WillFail(VariableList args, ScriptingEngine s)
		{
			NeedsToPass=false;
			return new NullVariable();
		}
		BaseVariable IsDebug(VariableList args, ScriptingEngine s)
		{
#if DEBUG
			return new BooleanVariable(true);
#else
			return new BooleanVariable(false);
#endif 
		}
		public TestStatus Test()
		{
			var status=new TestStatus();
			long start=DateTime.Now.Millisecond+(DateTime.Now.Second+DateTime.Now.Minute*60)*1000;
			status.Passed=true;
			try{
				Engine.Execute();
			}catch (AssertionFailedException e){
				status.Message=e.Message;
				status.Passed=false;
			}catch (Exception e){
				status.Message=e.Message;
				status.Passed=false;
			}
			
			if (!NeedsToPass){
				status.Passed=!status.Passed;
			}
			status.TimePassed=(DateTime.Now.Millisecond+(DateTime.Now.Second+DateTime.Now.Minute*60)*1000)-start;
			return status;
		}

	}
    }
}
