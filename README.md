# Scripter2

Scripter2 is a minimalistic scripting engine for .Net. It is completely interpreted and as such is automatically sandboxed.

# Platforms

It should run nearly anywhere there is .Net. Currently supported:

* .Net 3.5 and up
* WinRT/Windows Store
* Xbox360
* Windows Phone 7 and above
* Works under Mono

Because it is interpreted and doesn't rely on any reflection, it should work well on MonoDroid and friends as well. 

# Hello World

Because it's completely sandboxed, you must write functions to let it talk to the world. If you were to write a `Echo` function and expose it, this would be Hello World

    echo('Hello World');
    return;

This is an example Echo function implementation

	BaseVariable Echo(VariableList args, ScriptingEngine s){
		var msg=args.Get<string>(0);
		Console.WriteLine(msg);
		return new NullVariable();
	}

And then, to add it to your ScriptingEngine instance:

	var args=new VariableList();
	args.Add(new VariableContainer("message"));
	Engine.Global.Add(new VariableContainer("echo", new FunctionVariable(new ExternalScriptFunction(Echo), args)));

# Syntax

Hello world shows a bit of code and integration with the outside world, but doesn't really let you see some of it's powerful syntax features. A short demo of features:

Function Declaration - functions are first-class values. No lambdas yet however

	var foo=function(s){
	  echo(s); //or whatever
	}

	var tmp=function(meh){
	  meh(1)
	}
	tmp(function(i){ //anonymous functions
		echo(i);
	});

Tables - Arrays, Hashes, and friends (an example from regression tests)

	var t=[0 => 1, 2, 'foo' => 'bar', 3];
	if(t[0]!=2){
	 return 'wrong1';
	}
	if(t[1]!=3){
	  return 'wrong2';
	}
	if(t.foo!='bar'){
	  return 'wrong3';
	}
	return 'correct';

Casting/Types - Currently very limited type system, but easy to convert between them

    var s='1234';
	var i=50;
	return s as int + i;


# Licensing

Proudly BSD licensed. Use it in commercial projects or whatever! 

# Current Status

Still deep alpha state. Expect breaking changes both API-wise and changes in the actual language. I've matured a lot since this was initially created and as such I have a general regard that this code is crap... 

So, it's going to get changed to be more testable and I'm going to add some kind of way to make custom data types(probably via ducktyping/templating) 

