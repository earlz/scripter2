/*
<Copyright Header>
Copyright (c) 2012 Jordan "Earlz/hckr83" Earls  <http://lastyearswishes.com>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.
   
THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL
THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

</Copyright Header>
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Earlz.Scripter2.Core
{
	public enum VariableOperator
	{
		BinaryAdd,
		BinarySubtract,
		BinaryMultiply,
		BinaryDivide,
		BinaryModulo,
		BinaryAnd,
		BinaryOr,
		BinaryXor,
		BinaryShiftLeft,
		BinaryShiftRight,
		UnaryPlus,
		UnaryMinus,
		UnaryNot,
		SubScriptGet, 
		SubScriptSet, 
		KeyList,  
		KeyCount,
		ToString, 
		FunctionCall,
		FunctionArguments, 
		Clone, //used for templating later?
		Equal,
		LessThan,
		GreaterThan,
		Cast,
		LogicalAnd,
		LogicalOr,
		LogicalNot
	}
	public enum CoreTypes
	{
		Integer,
		Float,
		Boolean,
		Table,
		String,
		Null,
		Undefined,
		Function
	}

	
	
	public class VariableContainer
	{
		public BaseVariable Var;
		public string Name
		{
			get;
			set;
		}
		public VariableContainer(string name)
		{
			Name = name.ToLower();
		}
		public VariableContainer(string name, BaseVariable v)
		{
			Name = name.ToLower(); ;
			Var = v;
		}
	}
	
	public class VariableList : List<VariableContainer>
	{
		public VariableList() : base()
		{
		}
		public VariableContainer this[string name]
		{
			get
			{
				int tmp;
				name = name.ToLower();
				tmp = Find(name);
				if (tmp == -1)
				{
					throw new VariableNotFoundException("Could not find variable in VariableList");
				}
				return this[tmp];
			}
		}
        public bool Contains(string name)
        {
            return this.Any(x => x.Name == name);
        }
		public T Get<T>(int index)
		{
			var none=default(T);
			var t = typeof(T);
			BaseVariable v = this[index].Var;
			if (none == null)
			{
				if (v is NullVariable)
				{
					return none;
				}
			}
			if (t == typeof(int?) || t==typeof(int))
			{
				return (T)(object)((IntegerVariable)v.Cast(CoreTypes.Integer)).IntValue;
			}
			else if (t == typeof(bool?) || t==typeof(bool))
			{
				return (T)(object)((BooleanVariable)v.Cast(CoreTypes.Boolean)).BoolValue;
			}
			else if (t == typeof(decimal?) || t==typeof(decimal) || t==typeof(double?) || t==typeof(double))
			{
				return (T)(object)((FloatVariable)v.Cast(CoreTypes.Float)).FloatValue;
			}
			else if (t == typeof(string))
			{
				return (T)(object)((StringVariable)v.Cast(CoreTypes.String)).StringValue;
			}

			throw new ArgumentException("Type T is not supported", "T");
		}

		public T Get<T>(string name)
		{
			return Get<T>(Find(name));
		}

		public int Find(string name)
		{
			name = name.ToLower();
			int i;
			for (i = 0; i < this.Count; i++)
			{
				if (this[i].Name == name)
				{
					return i;
				}
			}
			return -1;
		}
	}
	
	abstract public class BaseVariable
	{
		public BaseVariable(object v){Value=v;}
		public BaseVariable(){}
		public CoreTypes CoreType;
		public int TypeID;
		/// <summary>
		/// The raw value of the variable
		/// </summary>
		public object Value;

		/// <summary>
		/// This will convert a native C# variable into an application ScriptType variable
		/// </summary>
		/// <param name="t"></param>
		/// <returns></returns>
		static public BaseVariable ConvertToVariable(object t)
		{
			if (t == null)
			{
				return new NullVariable();
			}
			else if (t is bool)
			{
				return new BooleanVariable((bool)t);
			}
			else if (t is Int16 || t is Int32)
			{
				return new IntegerVariable((int)t);
			}
			else if (t is double || t is float)
			{
				return new FloatVariable((double)t);
			}
			else if (t is decimal)
			{
				return new FloatVariable((double)t);
			}
			else if (t is Int64)
			{
				throw new NotSupportedException();
			}
			else
			{
				return new StringVariable(t.ToString());
			}
		}
		virtual public BaseVariable BinaryAdd(BaseVariable arg){
			throw new OperatorNotSupportedException(VariableOperator.BinaryAdd,this,arg);
		}
		virtual public BaseVariable BinarySubtract(BaseVariable arg){
			throw new OperatorNotSupportedException(VariableOperator.BinarySubtract,this,arg);
		}
		virtual public BaseVariable BinaryMultiply(BaseVariable arg){
			throw new OperatorNotSupportedException(VariableOperator.BinaryMultiply,this,arg);
		}
		virtual public BaseVariable BinaryDivide(BaseVariable arg){
			throw new OperatorNotSupportedException(VariableOperator.BinaryDivide,this,arg);
		}
		virtual public BaseVariable BinaryAnd(BaseVariable arg){
			throw new OperatorNotSupportedException(VariableOperator.BinaryAnd,this,arg);
		}
		virtual public BaseVariable BinaryModulo(BaseVariable arg){
			throw new OperatorNotSupportedException(VariableOperator.BinaryModulo,this,arg);
		}
		virtual public BaseVariable BinaryOr(BaseVariable arg){
			throw new OperatorNotSupportedException(VariableOperator.BinaryOr,this,arg);
		}
		virtual public BaseVariable BinaryXor(BaseVariable arg){
			throw new OperatorNotSupportedException(VariableOperator.BinaryXor,this,arg);
		}
		virtual public BaseVariable BinaryShiftLeft(BaseVariable arg){
			throw new OperatorNotSupportedException(VariableOperator.BinaryShiftLeft,this,arg);
		}
		virtual public BaseVariable BinaryShiftRight(BaseVariable arg){
			throw new OperatorNotSupportedException(VariableOperator.BinaryShiftRight,this,arg);
		}
		virtual public BaseVariable UnaryPlus(){
			throw new OperatorNotSupportedException(VariableOperator.UnaryPlus,this,null);
		}
		virtual public BaseVariable UnaryMinus(){
			throw new OperatorNotSupportedException(VariableOperator.UnaryMinus,this,null);
		}
		virtual public BaseVariable BitwiseNot(){
			throw new OperatorNotSupportedException(VariableOperator.UnaryNot,this,null);
		}
		//NOTE! Not sure of how to do short-cutting logical operators. 
		//Probably involves creating an "imaginary" ScriptValue that instead of having the value of a variable, instead is purpose made to actually
		//evaluate what is needed... and it'd only be called if needed. 
		virtual public BaseVariable LogicalAnd(BaseVariable arg){
			throw new OperatorNotSupportedException(VariableOperator.LogicalAnd,this,arg);
		}
		virtual public BaseVariable LogicalOr(BaseVariable arg){
			throw new OperatorNotSupportedException(VariableOperator.LogicalOr,this,arg);
		}
		virtual public BaseVariable LogicalNot(){
			throw new OperatorNotSupportedException(VariableOperator.LogicalNot,this,null);
		}
		virtual public BaseVariable SubScriptGet(BaseVariable key){
			throw new OperatorNotSupportedException(VariableOperator.SubScriptGet,this,key);
		}
		virtual public BaseVariable SubScriptSet(BaseVariable key,BaseVariable val){
			throw new OperatorNotSupportedException(VariableOperator.SubScriptSet, this, key);
		}
		virtual public BaseVariable KeyList(){
			throw new OperatorNotSupportedException(VariableOperator.KeyList,this,null);
		}
		virtual public BaseVariable KeyCount(){
			throw new OperatorNotSupportedException(VariableOperator.KeyCount,this,null);
		}
		virtual public BaseVariable OpToString(){
			throw new OperatorNotSupportedException(VariableOperator.ToString,this,null);
		}
		virtual public BaseVariable FunctionCall(InternalVariable args, InternalVariable script){
			throw new OperatorNotSupportedException(VariableOperator.FunctionCall,this,null);
		}
		virtual public InternalVariable FunctionArguments(){
			throw new OperatorNotSupportedException(VariableOperator.KeyList,this,null);
		}
		virtual public BaseVariable Clone(){
			throw new OperatorNotSupportedException(VariableOperator.Clone,this,null);
		}
		virtual public BaseVariable Equal(BaseVariable arg){
			throw new OperatorNotSupportedException(VariableOperator.Equal,this,arg);
		}	
		virtual public BaseVariable LessThan(BaseVariable arg){
			throw new OperatorNotSupportedException(VariableOperator.LessThan,this,arg);
		}
		virtual public BaseVariable GreaterThan(BaseVariable arg){
			throw new OperatorNotSupportedException(VariableOperator.GreaterThan,this,arg);
		}
		virtual public BaseVariable Cast(CoreTypes to)
		{
			throw new OperatorNotSupportedException(VariableOperator.Cast,this,new InternalVariable(to));
		}
        public override string ToString()
        {
            return "BaseVariable";
        }
	}
	public class BooleanVariable : BaseVariable
	{
		public bool BoolValue{
			get{
				return (bool)Value;
			}
			set{
				Value=value;
			}
		}
		public BooleanVariable(bool v){
			Value=v;
			CoreType=CoreTypes.Boolean;
		}
		public bool GetValue(){
			return (bool)Value;
		}
		public override BaseVariable BinaryAnd (BaseVariable arg)
		{
			return new BooleanVariable(BoolValue&(bool)((BooleanVariable)arg.Cast(CoreTypes.Boolean)).Value);
		}
		public override BaseVariable BinaryOr (BaseVariable arg)
		{
			return new BooleanVariable(BoolValue|(bool)((BooleanVariable)arg.Cast(CoreTypes.Boolean)).Value);
		}
		public override BaseVariable BinaryXor (BaseVariable arg)
		{
			return new BooleanVariable(BoolValue^(bool)((BooleanVariable)arg.Cast(CoreTypes.Boolean)).Value);
		}
		public override BaseVariable BitwiseNot ()
		{
			return new BooleanVariable(!BoolValue);
		}
		public override BaseVariable LogicalNot ()
		{
			return new BooleanVariable(!BoolValue);
		}
		public override BaseVariable Equal (BaseVariable arg)
		{
			return new BooleanVariable(BoolValue==(bool)((BooleanVariable)arg.Cast(CoreTypes.Boolean)).Value);
		}
		public override BaseVariable Cast (CoreTypes to)
		{
			switch(to){
			case CoreTypes.Boolean:
				return new BooleanVariable(BoolValue);
			case CoreTypes.Float:
				if(BoolValue){
					return new FloatVariable(1.0);
				}else{
					return new FloatVariable(0.0);
				}
			case CoreTypes.Integer:
				if(BoolValue){
					return new IntegerVariable(1);
				}else{
					return new IntegerVariable(0);
				}
			case CoreTypes.String:
				if(BoolValue){
					return new StringVariable("true");
				}else{
					return new StringVariable("false");
				}
			default:
				throw new IncompatibleCastException(this,to);
			}		
		}
        public override string ToString()
        {
            return BoolValue ? "true" : "false";
        }
		
	}
	public class NullVariable : BaseVariable
	{
		public NullVariable(){
			CoreType=CoreTypes.Null;
		}
        public override string ToString()
        {
            return "null";
        }
		
	}
	public class IntegerVariable : BaseVariable
	{
		public int IntValue{ //casting shortcut
			get{
				return (int)Value;
			}
			set{
				Value=value;
			}
		}
		
		public IntegerVariable(int v){
			Value=v;
			CoreType=CoreTypes.Integer;
		}
		public int GetValue(){
			return (int)Value;
		}
		public override BaseVariable BinaryAdd (BaseVariable arg)
		{
			return new IntegerVariable(IntValue+(int)((IntegerVariable)arg.Cast(CoreTypes.Integer)).Value);
		}
		public override BaseVariable BinarySubtract(BaseVariable arg)
		{
			return new IntegerVariable(IntValue-(int)((IntegerVariable)arg.Cast(CoreTypes.Integer)).Value);
		}
		public override BaseVariable BinaryMultiply(BaseVariable arg)
		{
			return new IntegerVariable(IntValue*(int)((IntegerVariable)arg.Cast(CoreTypes.Integer)).Value);
		}
		public override BaseVariable BinaryDivide(BaseVariable arg)
		{
			return new IntegerVariable(IntValue/(int)((IntegerVariable)arg.Cast(CoreTypes.Integer)).Value);
		}
		public override BaseVariable BinaryModulo(BaseVariable arg)
		{
			return new IntegerVariable(IntValue%(int)((IntegerVariable)arg.Cast(CoreTypes.Integer)).Value);
		}
		public override BaseVariable Cast (CoreTypes to)
		{
			switch(to){
			case CoreTypes.Boolean:
				if(IntValue==0){
					return new BooleanVariable(false);
				}else{
					return new BooleanVariable(true);
				}
			case CoreTypes.Float:
				return new FloatVariable(IntValue);
			case CoreTypes.Integer:
				return new IntegerVariable(IntValue);
			case CoreTypes.String:
				return new StringVariable(IntValue.ToString());
			default:
				throw new IncompatibleCastException(this,to);
			}	
		}
		public override BaseVariable Equal (BaseVariable arg)
		{
			return new BooleanVariable(IntValue==(int)((IntegerVariable)arg.Cast(CoreTypes.Integer)).Value);
		}
		public override BaseVariable GreaterThan(BaseVariable arg)
		{
			return new BooleanVariable(IntValue>(int)((IntegerVariable)arg.Cast(CoreTypes.Integer)).Value);
		}
		public override BaseVariable LessThan(BaseVariable arg)
		{
			return new BooleanVariable(IntValue<(int)((IntegerVariable)arg.Cast(CoreTypes.Integer)).Value);
		}
		public override BaseVariable BinaryAnd (BaseVariable arg)
		{
			return new IntegerVariable(IntValue&(int)((IntegerVariable)arg.Cast(CoreTypes.Integer)).Value);
		}
		public override BaseVariable BinaryOr (BaseVariable arg)
		{
			return new IntegerVariable(IntValue|(int)((IntegerVariable)arg.Cast(CoreTypes.Integer)).Value);
		}
		public override BaseVariable BinaryShiftLeft (BaseVariable arg)
		{
			return new IntegerVariable(IntValue<<(int)((IntegerVariable)arg.Cast(CoreTypes.Integer)).Value);
		}
		public override BaseVariable BinaryShiftRight (BaseVariable arg)
		{
			return new IntegerVariable(IntValue>>(int)((IntegerVariable)arg.Cast(CoreTypes.Integer)).Value);
		}
		public override BaseVariable BinaryXor (BaseVariable arg)
		{
			return new IntegerVariable(IntValue^(int)((IntegerVariable)arg.Cast(CoreTypes.Integer)).Value);
		}
		public override BaseVariable BitwiseNot ()
		{
			return new IntegerVariable(~IntValue);
		}
		public override BaseVariable UnaryMinus ()
		{
			return new IntegerVariable(-IntValue);
		}
		public override BaseVariable Clone ()
		{
			return new InternalVariable(IntValue);
		}
		public override BaseVariable UnaryPlus ()
		{
			return new IntegerVariable(+IntValue);
		}
		public override BaseVariable OpToString ()
		{
			return new StringVariable(IntValue.ToString());
		}
        public override string ToString()
        {
            return IntValue.ToString();
        }
	}
	public class TableVariable : BaseVariable
	{
		public Dictionary<BaseVariable, BaseVariable> Table=new Dictionary<BaseVariable, BaseVariable>();
		public TableVariable(object v){
			CoreType=CoreTypes.Table;
			Value=v;
		}
		public TableVariable(){
			CoreType=CoreTypes.Table;
		}
		public Dictionary<BaseVariable,BaseVariable> GetValue(){
			return (Dictionary<BaseVariable,BaseVariable>)Value;
		}
		public override BaseVariable SubScriptSet (BaseVariable key, BaseVariable val)
		{
			var k=FindRealKey(key);
			if(k!=null){
				Table[k]=val;
			}else{
				Table.Add(key, val);
			}
			return this; //correct? or should this be `val`?
		}
		public override BaseVariable SubScriptGet (BaseVariable key)
		{
			var k=FindRealKey(key);
			if(k!=null){
				return Table[k];
			}else{
				return new UndefinedVariable();
			}
		}
		/*
		 * This function is required because even though the contents of a "variable" may be the same,
		 * this doesn't mean it's "reference value" or memory-address-ish thing is the same. 
		 * Hopefully this function will be able to be eliminated or sped up in the future
		 */
		protected BaseVariable FindRealKey(BaseVariable key)
		{
			foreach(var k in Table.Keys)
			{
				if(key.CoreType==k.CoreType){
					if(((BooleanVariable)k.Equal(key)).BoolValue) 
					{
						return k;
					}
				}
			}
			return null;
		}
		public override BaseVariable KeyList ()
		{
			var tmp=new TableVariable();
			int i=0;
			foreach(var key in Table.Keys){
				if(key is StringVariable){
					var s=(StringVariable) key;
					if(s.StringValue.Length>0 && s.StringValue[0]=='_'){
						continue;
					}
				}
				tmp.Table.Add(new IntegerVariable(i), key);
				i++;
			}
			return tmp;
		}
		public override BaseVariable KeyCount ()
		{
			int i=0;
			foreach(var key in Table.Keys){
				if(key is StringVariable){
					var s=(StringVariable) key;
					if(s.StringValue.Length>0 && s.StringValue[0]=='_'){
						continue;
					}
				}
				i++;
			}
			return new IntegerVariable(i);
		}
        int RecursionDetection = 0;
        public override string ToString()
        {
            if (RecursionDetection > 1)
            {
                return "!!Recursive!!";
            }
            StringBuilder sb = new StringBuilder(Table.Count*6);
            RecursionDetection++;
            sb.Append("[");
            foreach (var key in Table.Keys)
            {
                sb.Append(key.ToString());
                sb.Append(" => ");
                sb.Append(Table[key].ToString());
                sb.Append(", ");
            }
            sb.Remove(sb.Length-2, 2); //remove ending comma and space
            sb.Append("]");
            RecursionDetection--;
            return sb.ToString();
        }
		
	}
	public class FloatVariable : BaseVariable
	{
		public double FloatValue{
			get{
				return (double)Value;
			}
			set{
				Value=value;
			}
		}
		public FloatVariable(double v){
			CoreType=CoreTypes.Float;
			Value=v;
		}
		public double GetValue(){
			return (double)Value;
		}
		public override BaseVariable Cast (CoreTypes to)
		{
			switch(to){
			case CoreTypes.Boolean:
				if(FloatValue==0.0){
					return new BooleanVariable(false);
				}else{
					return new BooleanVariable(true);
				}
			case CoreTypes.Float:
				return new FloatVariable(FloatValue);
			case CoreTypes.Integer:
				return new IntegerVariable((int)FloatValue);
			case CoreTypes.String:
				return new StringVariable(FloatValue.ToString());
			default:
				throw new IncompatibleCastException(this,to);
			}	
		}
        public override string ToString()
        {
            return FloatValue.ToString();
        }
	}
	public class StringVariable : BaseVariable
	{
		public string StringValue{
			get{
				return (string)Value;
			}
			set{
				Value=value;
			}
		}
		public StringVariable(string v){
			CoreType=CoreTypes.String;
			Value=v;
		}
		public string GetValue(){
			return (string) Value;
		}
		public override BaseVariable Equal (BaseVariable arg)
		{
			return new BooleanVariable(StringValue == (string)((StringVariable)arg.Cast(CoreTypes.String)).StringValue);
		}
		public override BaseVariable BinaryAdd (BaseVariable arg)
		{
			return new StringVariable(StringValue + (string)((StringVariable)arg.Cast(CoreTypes.String)).StringValue);
		}
		public override BaseVariable Cast (CoreTypes to)
		{
			switch(to){
			case CoreTypes.Boolean:
				if(StringValue.ToLower()=="true" || StringValue=="1"){
					return new BooleanVariable(true);
				}else{
					return new BooleanVariable(false);
				}
			case CoreTypes.Float:
				double f;
				if(double.TryParse(StringValue, out f)){
					return new FloatVariable(f);
				}else{
					return new NullVariable(); //should it be like this?
				}
			case CoreTypes.Integer:
				int i;
				if(int.TryParse(StringValue, out i)){
					return new IntegerVariable(i);
				}else{
					return new NullVariable();
				}
			case CoreTypes.String:
				return new StringVariable(StringValue);
			default:
				throw new IncompatibleCastException(this,to);
			}	
		}
        public override string ToString()
        {
            return string.Format("\"{0}\"", StringValue);
        }
	}
	public class UndefinedVariable : BaseVariable
	{
		public UndefinedVariable()
		{
			CoreType=CoreTypes.Undefined;
		}
		public override BaseVariable Equal (BaseVariable arg)
		{
			if(arg is UndefinedVariable){
				return new BooleanVariable(true);
			}else{
				return new BooleanVariable(false);
			}
		}
        public override string ToString()
        {
            return "undefined";
        }
	}
	public class ScopeMarkerVariable : InternalVariable
	{
		public ScopeMarkerVariable() : base() { Value = 0; }
		public ScopeMarkerVariable(string v) : base(v) { }
	}
	public class InternalVariable : BaseVariable
	{
		public InternalVariable() : base() { Value = false; }
		public InternalVariable(object v) : base(v) { }
	}
	public delegate BaseVariable ExternalScriptFunction(VariableList args, ScriptingEngine s);
	public class FunctionVariable : BaseVariable
	{
		VariableList Arguments;
		int Start;
		VariableList Local; //for closures?
		ExternalScriptFunction External;
		//add built-ins
		public FunctionVariable(int start, VariableList args){
			Arguments=args;
			Start=start;
			CoreType=CoreTypes.Function;
		}
		public FunctionVariable(ExternalScriptFunction func, VariableList args){
			External=func;
			Arguments=args;
			CoreType=CoreTypes.Function;
		}
		public override InternalVariable FunctionArguments ()
		{
			return new InternalVariable(Arguments);
		}
		public override BaseVariable FunctionCall (InternalVariable args, InternalVariable script)
		{
			if(External!=null){
				return External((VariableList)args.Value,(ScriptingEngine)script.Value) ?? new NullVariable();
			}else{
				ScriptingEngine s=(ScriptingEngine)script.Value;
				return s.Execute(Start,(VariableList)args.Value).LastResult as BaseVariable ?? new NullVariable();
			}
		}
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("function(");
            foreach (var arg in Arguments)
            {
                sb.Append(arg.Name);
                sb.Append(", ");
            }
            sb.Remove(sb.Length - 2, 2);
            sb.Append(")");
            return sb.ToString();
        }
		
	}
}

