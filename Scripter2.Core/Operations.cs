/*
<Copyright Header>
Copyright (c) 2012 Jordan "Earlz/hckr83" Earls  <http://lastyearswishes.com>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.
   
THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL
THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

</Copyright Header>
*/
using System;
using Earlz.SimplyExpress;

namespace Earlz.Scripter2.Core
{
	
	public class LogicalNotOperation : Operation
	{
		public LogicalNotOperation()
		{
			OpType=OperationType.RightOp;
			Name="LogicalNot";
			Precedence=3;
		}
		public override ExpressionValue Execute (ExpressionValue left, ExpressionValue right)
		{
			ScriptValue r=(ScriptValue)right;
			r.Value.Var=r.Value.Var.LogicalNot();
			return new ScriptValue(r.Position, r.Script, r.Value);
		}
	}
	public class BitwiseNotOperation : Operation
	{
		public BitwiseNotOperation()
		{
			OpType=OperationType.RightOp;
			Name="BitwiseNot";
			Precedence=3;
		}
		public override ExpressionValue Execute (ExpressionValue left, ExpressionValue right)
		{
			ScriptValue r=(ScriptValue)right;
			r.Value.Var=r.Value.Var.BitwiseNot();
			return new ScriptValue(r.Position, r.Script, r.Value);
		}
	}
	
	public class BinaryAddOperation: Operation
	{
		public BinaryAddOperation()
		{
			OpType=OperationType.LeftRightOp;
			Name="BinaryAdd";
			Precedence=6;
		}
		public override ExpressionValue Execute (ExpressionValue left, ExpressionValue right)
		{
			ScriptValue l=(ScriptValue)left;
			ScriptValue r=(ScriptValue)right;
			return new ScriptValue(l.Position,l.Script,new VariableContainer("",l.Value.Var.BinaryAdd(r.Value.Var)));
		}
	}
	public class BinarySubtractOperation: Operation
	{
		public BinarySubtractOperation()
		{
			OpType=OperationType.LeftRightOp;
			Name="BinarySubtract";
			Precedence=6;
		}
		public override ExpressionValue Execute (ExpressionValue left, ExpressionValue right)
		{
			ScriptValue l=(ScriptValue)left;
			ScriptValue r=(ScriptValue)right;
			return new ScriptValue(l.Position,l.Script,new VariableContainer("",l.Value.Var.BinarySubtract(r.Value.Var)));
		}
	}
	public class BinaryMultiplyOperation: Operation
	{
		public BinaryMultiplyOperation()
		{
			OpType=OperationType.LeftRightOp;
			Name="BinaryMultiply";
			Precedence=5;
		}
		public override ExpressionValue Execute (ExpressionValue left, ExpressionValue right)
		{
			ScriptValue l=(ScriptValue)left;
			ScriptValue r=(ScriptValue)right;
			return new ScriptValue(l.Position, l.Script,new VariableContainer("",l.Value.Var.BinaryMultiply(r.Value.Var)));
		}
	}
	public class BinaryDivideOperation: Operation
	{
		public BinaryDivideOperation()
		{
			OpType=OperationType.LeftRightOp;
			Name="BinaryDivide";
			Precedence=5;
		}
		public override ExpressionValue Execute (ExpressionValue left, ExpressionValue right)
		{
			ScriptValue l=(ScriptValue)left;
			ScriptValue r=(ScriptValue)right;
			return new ScriptValue(l.Position, l.Script,new VariableContainer("",l.Value.Var.BinaryDivide(r.Value.Var)));
		}
	}
	public class BinaryModuloOperation: Operation
	{
		public BinaryModuloOperation()
		{
			OpType=OperationType.LeftRightOp;
			Name="BinaryModulo";
			Precedence=5;
		}
		public override ExpressionValue Execute (ExpressionValue left, ExpressionValue right)
		{
			ScriptValue l=(ScriptValue)left;
			ScriptValue r=(ScriptValue)right;
			return new ScriptValue(l.Position, l.Script,new VariableContainer("",l.Value.Var.BinaryModulo(r.Value.Var)));
		}
	}
	public class AssignmentOperation : Operation
	{
		public AssignmentOperation()
		{
			OpType=OperationType.LeftRightOp;
			Name="Assignment";
			Precedence=16;
		}
		public override ExpressionValue Execute (ExpressionValue left, ExpressionValue right)
		{
			ScriptValue l=(ScriptValue)left;
			ScriptValue r=(ScriptValue)right;
			if(l.Value.Name==""){
				throw new SyntaxException("Assignment to a constant value",l.Position);
			}
			if(l.Table != null){
				l.Table.SubScriptSet(l.Key, r.Value.Var);
			}
			l.Value.Var=r.Value.Var;
			return new ScriptValue(l.Position, l.Script,l.Value);
		}
	}
	public class FunctionCallOperation : Operation
	{
		VariableList Arguments;
		public FunctionCallOperation(VariableList args)
		{
			Arguments=args;
			OpType=OperationType.LeftOp;
			Name="FunctionCall";
			Precedence=2;
		}
		public override ExpressionValue Execute (ExpressionValue left, ExpressionValue right)
		{
			ScriptValue l=(ScriptValue)left;
			FunctionVariable f=(FunctionVariable)l.Value.Var;
			VariableList tmp=(VariableList)f.FunctionArguments().Value;
			//FIXME possibly?
			if(tmp.Count!=Arguments.Count){
				throw new RuntimeException("Function call argument count mis-match", l.Position);
			}
			for(int i=0;i<Arguments.Count;i++){
                Arguments[i].Name = tmp[i].Name;
			}
            Arguments.Add(new VariableContainer("this", l.Table));
			var res= new ScriptValue(l.Position, l.Script,
			     new VariableContainer("",f.FunctionCall(new InternalVariable(Arguments), new InternalVariable(l.Script))));
			for(int i=0;i<tmp.Count;i++){
				tmp[i].Var=null; //don't hold on to possibly huge temporary variables that can't be accessed
                Arguments[i].Var = null;
			}
			return res;
		}
	}
	public class PreIncrementOperation : Operation
	{
		public PreIncrementOperation()
		{
			OpType=OperationType.RightOp;
			Name="PrefixIncrement";
			Precedence=3;
		}
		public override ExpressionValue Execute (ExpressionValue left, ExpressionValue right)
		{
			ScriptValue r=(ScriptValue)right;
			r.Value.Var=r.Value.Var.BinaryAdd(new IntegerVariable(1));
			if(r.Table != null){
				r.Table.SubScriptSet(r.Key, r.Value.Var);
			}
			return new ScriptValue(r.Position, r.Script, r.Value);
		}
	}
	public class PostIncrementOperation : Operation
	{
		public PostIncrementOperation()
		{
			OpType=OperationType.LeftOp;
			Name="PostfixIncrement";
			Precedence=2;
		}
		public override ExpressionValue Execute (ExpressionValue left, ExpressionValue right)
		{
			ScriptValue l=(ScriptValue)left;
			BaseVariable tmp;
			tmp=l.Value.Var;
			l.Value.Var=l.Value.Var.BinaryAdd(new IntegerVariable(1));
			if(l.Table != null){
				l.Table.SubScriptSet(l.Key, l.Value.Var);
			}	
			return new ScriptValue(l.Position, l.Script, new VariableContainer("",tmp));
		}
	}
	public class EqualOperation : Operation
	{
		public EqualOperation()
		{
			OpType=OperationType.LeftRightOp;
			Name="IsEqual";
			Precedence=9;
		}
		public override ExpressionValue Execute (ExpressionValue left, ExpressionValue right)
		{
			ScriptValue l=(ScriptValue)left;
			ScriptValue r=(ScriptValue)right;
			BaseVariable tmp;
			tmp=l.Value.Var.Equal(r.Value.Var);
			return new ScriptValue(l.Position, l.Script, new VariableContainer("",tmp));
		}
	}
	public class GreaterThanOperation : Operation
	{
		public GreaterThanOperation()
		{
			OpType=OperationType.LeftRightOp;
			Name="IsGreater";
			Precedence=8;
		}
		public override ExpressionValue Execute (ExpressionValue left, ExpressionValue right)
		{
			ScriptValue l=(ScriptValue)left;
			ScriptValue r=(ScriptValue)right;
			BaseVariable tmp;
			tmp=l.Value.Var.GreaterThan(r.Value.Var);
			return new ScriptValue(l.Position, l.Script, new VariableContainer("",tmp));
		}
	}
	public class LessThanOperation : Operation
	{
		public LessThanOperation()
		{
			OpType=OperationType.LeftRightOp;
			Name="IsLess";
			Precedence=8;
		}
		public override ExpressionValue Execute (ExpressionValue left, ExpressionValue right)
		{
			ScriptValue l=(ScriptValue)left;
			ScriptValue r=(ScriptValue)right;
			BaseVariable tmp;
			tmp=l.Value.Var.LessThan(r.Value.Var);
			return new ScriptValue(l.Position, l.Script, new VariableContainer("",tmp));
		}
	}
	public class NotEqualOperation : Operation
	{
		public NotEqualOperation()
		{
			OpType=OperationType.LeftRightOp;
			Name="NotEqual";
			Precedence=9;
		}
		public override ExpressionValue Execute (ExpressionValue left, ExpressionValue right)
		{
			ScriptValue l=(ScriptValue)left;
			ScriptValue r=(ScriptValue)right;
			BaseVariable tmp;
			tmp=l.Value.Var.Equal(r.Value.Var);
			tmp=tmp.LogicalNot();
			return new ScriptValue(l.Position, l.Script, new VariableContainer("",tmp));
		}
	}
	public class LessThanOrEqualOperation : Operation
	{
		public LessThanOrEqualOperation()
		{
			OpType=OperationType.LeftRightOp;
			Name="IsLessOrEqual";
			Precedence=8;
		}
		public override ExpressionValue Execute (ExpressionValue left, ExpressionValue right)
		{
			ScriptValue l=(ScriptValue)left;
			ScriptValue r=(ScriptValue)right;
			BaseVariable tmp;
			tmp=l.Value.Var.LessThan(r.Value.Var); //this could be changed to be faster, but it'd involve preventing some operator overloading flexibility
			tmp=tmp.BinaryOr(tmp.Equal(r.Value.Var));
			return new ScriptValue(l.Position, l.Script, new VariableContainer("",tmp));
		}
	}
	public class GreaterThanOrEqualOperation : Operation
	{
		public GreaterThanOrEqualOperation()
		{
			OpType=OperationType.LeftRightOp;
			Name="IsGreaterOrEqual";
			Precedence=8;
		}
		public override ExpressionValue Execute (ExpressionValue left, ExpressionValue right)
		{
			ScriptValue l=(ScriptValue)left;
			ScriptValue r=(ScriptValue)right;
			BaseVariable tmp;
			tmp=l.Value.Var.GreaterThan(r.Value.Var); //this could be changed to be faster, but it'd involve preventing some operator overloading flexibility
			tmp=tmp.BinaryOr(tmp.Equal(r.Value.Var));
			return new ScriptValue(l.Position, l.Script, new VariableContainer("",tmp));
		}
	}
	public class CastOperation : Operation
	{
		CoreTypes type;
		public CastOperation(CoreTypes t)
		{
			OpType=OperationType.LeftOp;
			Name="CastOperation";
			Precedence=2;
			type=t;
		}
		public override ExpressionValue Execute (ExpressionValue left, ExpressionValue right)
		{
			ScriptValue l=(ScriptValue)left;
			BaseVariable tmp;
			tmp=l.Value.Var.Cast(type);
			return new ScriptValue(l.Position, l.Script, new VariableContainer("", tmp));
		}
	}
	public class IsTypeOperation : Operation
	{
		CoreTypes type;
		public IsTypeOperation(CoreTypes t)
		{
			OpType=OperationType.LeftOp;
			Name="IsTypeOperation";
			Precedence=2;
			type=t;
		}
		public override ExpressionValue Execute (ExpressionValue left, ExpressionValue right)
		{
			ScriptValue l=(ScriptValue)left;
			var v=l.Value.Var;
			BaseVariable tmp;
			tmp=new BooleanVariable(v.CoreType==type);
			return new ScriptValue(l.Position, l.Script, new VariableContainer("", tmp));
		}
	}
	public class SubscriptOperation : Operation
	{
		BaseVariable Key;
		public SubscriptOperation(BaseVariable key)
		{
			OpType=OperationType.LeftOp;
			Name="SubscriptOperation";
			Precedence=2;
			Key=key;
		}
		public override ExpressionValue Execute (ExpressionValue left, ExpressionValue right)
		{
			ScriptValue l=(ScriptValue)left;
			var v=l.Value.Var;
			BaseVariable tmp;
			tmp=l.Value.Var.SubScriptGet(Key);
			var s = new ScriptValue(l.Position, l.Script, new VariableContainer(l.Value.Name, tmp));
			s.Key=Key;
			s.Table=(TableVariable)l.Value.Var;
			return s;
		}
	}
	
	public class ShiftLeftOperation: Operation
	{
		public ShiftLeftOperation()
		{
			OpType=OperationType.LeftRightOp;
			Name="ShiftLeft";
			Precedence=7;
		}
		public override ExpressionValue Execute (ExpressionValue left, ExpressionValue right)
		{
			ScriptValue l=(ScriptValue)left;
			ScriptValue r=(ScriptValue)right;
			return new ScriptValue(l.Position, l.Script,new VariableContainer("",l.Value.Var.BinaryShiftLeft(r.Value.Var)));
		}
	}
	public class ShiftRightOperation: Operation
	{
		public ShiftRightOperation()
		{
			OpType=OperationType.LeftRightOp;
			Name="ShiftRight";
			Precedence=7;
		}
		public override ExpressionValue Execute (ExpressionValue left, ExpressionValue right)
		{
			ScriptValue l=(ScriptValue)left;
			ScriptValue r=(ScriptValue)right;
			return new ScriptValue(l.Position, l.Script,new VariableContainer("",l.Value.Var.BinaryShiftRight(r.Value.Var)));
		}
	}
	public class PreDecrementOperation : Operation
	{
		public PreDecrementOperation()
		{
			OpType=OperationType.RightOp;
			Name="PrefixDecrement";
			Precedence=3;
		}
		public override ExpressionValue Execute (ExpressionValue left, ExpressionValue right)
		{
			ScriptValue r=(ScriptValue)right;
			r.Value.Var=r.Value.Var.BinarySubtract(new IntegerVariable(1));
			if(r.Table != null){
				r.Table.SubScriptSet(r.Key, r.Value.Var);
			}
			return new ScriptValue(r.Position, r.Script, r.Value);
		}
	}
	public class PostDecrementOperation : Operation
	{
		public PostDecrementOperation()
		{
			OpType=OperationType.LeftOp;
			Name="PostfixDecrement";
			Precedence=2;
		}
		public override ExpressionValue Execute (ExpressionValue left, ExpressionValue right)
		{
			ScriptValue l=(ScriptValue)left;
			BaseVariable tmp;
			tmp=l.Value.Var;
			l.Value.Var=l.Value.Var.BinarySubtract(new IntegerVariable(1));
			if(l.Table != null){
				l.Table.SubScriptSet(l.Key, l.Value.Var);
			}
			return new ScriptValue(l.Position, l.Script, new VariableContainer("",tmp));
		}
	}	
	public class UnaryMinusOperation : Operation
	{
		public UnaryMinusOperation()
		{
			OpType=OperationType.RightOp;
			Name="UnaryMinus";
			Precedence=3;
		}
		public override ExpressionValue Execute (ExpressionValue left, ExpressionValue right)
		{
			ScriptValue r=(ScriptValue)right;
			r.Value.Var=r.Value.Var.UnaryMinus();
			return new ScriptValue(r.Position, r.Script, r.Value);
		}
	}
	public class UnaryPlusOperation : Operation
	{
		public UnaryPlusOperation()
		{
			OpType=OperationType.RightOp;
			Name="UnaryPlus";
			Precedence=3;
		}
		public override ExpressionValue Execute (ExpressionValue left, ExpressionValue right)
		{
			ScriptValue r=(ScriptValue)right;
			r.Value.Var=r.Value.Var.UnaryPlus();
			return new ScriptValue(r.Position, r.Script, r.Value);
		}
	}
	public class BinaryAndOperation: Operation
	{
		public BinaryAndOperation()
		{
			OpType=OperationType.LeftRightOp;
			Name="BinaryAnd";
			Precedence=10;
		}
		public override ExpressionValue Execute (ExpressionValue left, ExpressionValue right)
		{
			ScriptValue l=(ScriptValue)left;
			ScriptValue r=(ScriptValue)right;
			return new ScriptValue(l.Position,l.Script,new VariableContainer("",l.Value.Var.BinaryAnd(r.Value.Var)));
		}
	}
	public class BinaryOrOperation: Operation
	{
		public BinaryOrOperation()
		{
			OpType=OperationType.LeftRightOp;
			Name="BinaryOr";
			Precedence=12;
		}
		public override ExpressionValue Execute (ExpressionValue left, ExpressionValue right)
		{
			ScriptValue l=(ScriptValue)left;
			ScriptValue r=(ScriptValue)right;
			return new ScriptValue(l.Position,l.Script,new VariableContainer("",l.Value.Var.BinaryOr(r.Value.Var)));
		}
	}
	public class BinaryXorOperation: Operation
	{
		public BinaryXorOperation()
		{
			OpType=OperationType.LeftRightOp;
			Name="BinaryXor";
			Precedence=11;
		}
		public override ExpressionValue Execute (ExpressionValue left, ExpressionValue right)
		{
			ScriptValue l=(ScriptValue)left;
			ScriptValue r=(ScriptValue)right;
			return new ScriptValue(l.Position,l.Script,new VariableContainer("",l.Value.Var.BinaryXor(r.Value.Var)));
		}
	}
}

