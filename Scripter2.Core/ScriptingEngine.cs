﻿/*
Copyright (c) 2012-2013 Jordan "Earlz" Earls  <http://earlz.net>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.
   
THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL
THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Earlz.SimplyExpress;
using System.Diagnostics;
using System.Threading;


namespace Earlz.Scripter2.Core
{

    public class ScriptingEngine
    {
		protected string Script;
		List<SyntaxElement> Tokens;
		int RecursionCount=0;
		public VariableList Global
		{
			get;
			protected set;
		}
		static readonly string[] Keywords = {
			"var","return","yield","function","if","foreach", "as", "islike",
			"null", "nil", "undefined", "while", "else", "global", "while"
		};
		public ScriptingEngine(string script)
		{
			Global=new VariableList();
			Script=script;
			var prep=new Prepare(script);
			Tokens=prep.Tokens;
			AddBuiltinFunctions();
		}
        public ScriptingEngine()
        {
            Global = new VariableList();
            Tokens = new List<SyntaxElement>();
			AddBuiltinFunctions();
        }
		public SyntaxElement GetElement(int pos){
			return Tokens[pos];
		}
		public void AddBuiltinFunctions(){ //will add the built in functions such as `isdefined`
			var args=new VariableList();
			args.Add(new VariableContainer("variable"));
			Global.Add(new VariableContainer("isdefined", new FunctionVariable(new ExternalScriptFunction(BuiltinIsDefined), args)));
			Global.Add(new VariableContainer("keylist", new FunctionVariable(new ExternalScriptFunction(BuiltinKeyList), args)));
			Global.Add(new VariableContainer("keycount", new FunctionVariable(new ExternalScriptFunction(BuiltinKeyCount), args)));
		}
		BaseVariable BuiltinIsDefined(VariableList args, ScriptingEngine s){
			if(args["variable"].Var is UndefinedVariable){
				return new BooleanVariable(false);
			}else{
				return new BooleanVariable(true);
			}
		}
		BaseVariable BuiltinKeyList(VariableList args, ScriptingEngine s){
			return args[0].Var.KeyList();
		}
		BaseVariable BuiltinKeyCount(VariableList args, ScriptingEngine s){
			return args[0].Var.KeyCount();
		}
		public void KeywordVar (ref int pos, VariableList local)
		{
			//var x; and var x=10;
			pos++; 
			if(Tokens[pos].Hint != ElementHint.Identifier || Keywords.Contains(Tokens[pos].Element)){
				throw new SyntaxException("expected valid identifier for new variable name",pos);
			}
			var vc=new VariableContainer(Tokens[pos].Element);
			foreach(var item in local){ //note, not quite correct. Doesn't take scope into account
				if(item.Name==vc.Name){
					throw new RuntimeException("Duplicate variable name!", pos); 
				}
			}
			vc.Var=new UndefinedVariable(); //for type correctness
			local.Add(vc);
			
			pos++;
			if(Tokens[pos].Element==";"){
				return;
			}
			pos-=2; //otherwise it's `var x=10;` so just set it so that the engine will see this as `x=10;` after this function 
		}
		public BaseVariable KeywordReturn(ref int pos, VariableList local)
		{
			pos++;
			if(Tokens[pos].Element==";"){
				return null;
			}else{
				return EvaluateExpression(ref pos, local, new string[]{";"});
			}
		}
		BaseVariable KeywordIf(ref int pos, VariableList local){
			pos++; //get past if
			if(Tokens[pos].Element!="("){
				throw new SyntaxException("Expecting `(` after if statement", pos);
			}
			pos++;
			var cond=EvaluateExpression(ref pos, local, new string[]{")"});
			bool c=false;
			if(cond is BooleanVariable){
				c=((BooleanVariable)cond).GetValue();
			}
			if(c){
				pos++;
				if(Tokens[pos].Element=="{"){
					CreateScope(pos, local);
					pos++;
					var res=Execute(pos,local);
					DeleteScope(pos, local);
					if(res!=null){
						return res.LastResult;
					}
					int len=BlockLength(pos);
					pos+=len;
					pos++;
				}else{
					throw new NotImplementedException("Single-statement conditionals aren't supported yet. Use `{}` to encompass the statement"); //for now for consistency
					//code below is wrong anyway. Can't cover `return`
					EvaluateExpression(ref pos,local, new string[]{";"});
					pos++;
				}
				if(Tokens[pos].Element=="else"){
					//need to skip this
					pos++;
					if(Tokens[pos].Element=="{"){
						pos++;
						pos+=BlockLength(pos);
					}else{
						throw new NotImplementedException("Single-statement conditionals aren't supported yet. Use `{}` to encompass the statement"); //this is super hard to parse. 
						//not for sure of how this will end up being implemented
					}
				}else{
					pos--;
				}
			}else{
				pos++;
				if(Tokens[pos].Element=="{"){
					pos++;
					pos+=BlockLength(pos);
					pos++;
				}else{
					throw new NotImplementedException("Single-statement conditionals aren't supported yet. Use `{}` to encompass the statement");
				}
				if(Tokens[pos].Element=="else"){
					pos++;
					if(Tokens[pos].Element=="{"){
						CreateScope(pos, local);
						pos++;
						var res=Execute(pos,local);
						DeleteScope(pos, local);
						if(res!=null){
							return res.LastResult;
						}
						int len=BlockLength(pos);
						pos+=len;
					}else{
						if(Tokens[pos].Element=="if"){
							throw new NotImplementedException("still not supported :( ");
							KeywordIf(ref pos, local);
						}else{
							throw new NotImplementedException("Single-statement conditionals aren't supported yet. Use `{}` to encompass the statement"); //this is super hard to parse. 
						}
						//not for sure of how this will end up being implemented
					}
				}else{
					pos--;
				}
			}
			return null;
		}
		BaseVariable KeywordWhile(ref int pos, VariableList local){
			pos++; //get past while
			if(Tokens[pos].Element!="("){
				throw new SyntaxException("Expecting `(` after while statement", pos);
			}
			pos++;
			bool c=true;
			int conditionpos=pos;
			while(c){
				pos=conditionpos;
				c=false;
				var cond=EvaluateExpression(ref pos, local, new string[]{")"});
				if(cond is BooleanVariable){
					c=((BooleanVariable)cond).GetValue();
				}
				//need a "skip-expression" type function for 'inline' type loops and if statements
				if(c){
					pos++;
					if(Tokens[pos].Element=="{"){
						CreateScope(pos, local);
						pos++;
						var res=Execute(pos,local);
						DeleteScope(pos, local);
						if(res!=null){
							DeleteScope(pos, local);
							return res.LastResult;
						}
						int len=BlockLength(pos);
						pos+=len;
					}else{
						throw new NotImplementedException("Single expression statements aren't supported yet. Use `{}` to encase the statement"); //for now for consistency
					}
				}else{
					pos++;
					if(Tokens[pos].Element=="{"){
						pos++;
						pos+=BlockLength(pos);
					}else{
						throw new NotImplementedException("Single expression statements aren't supported yet. Use `{}` to encase the statement");
					}
				}
			}
			return null;
		}
        protected int ChunkPos=0;
        VariableList ChunkVariables = new VariableList();
        public string ExecuteChunk(string chunk)
        {
            Script += "\n"+chunk;
            return ExecuteChunk(new Prepare(chunk).Tokens);
        }
        public string ExecuteChunk(List<SyntaxElement> tokens)
        {
            RecursionCount = 0;
            Tokens.AddRange(tokens); //Someone scream LOH fragmentation :( 
            var res=Execute(ChunkPos, ChunkVariables);
            ChunkPos = res.Position;
            return "=> "+res.LastResult.ToString();
        }
    	public object Execute(){
			return Execute(0,new VariableList());
		}
		public ScriptState Execute(int pos, VariableList local)
		{
			int CurlyCount=0;
			RecursionCount++;
			if(RecursionCount>Config.RECURSION_LIMIT){
				throw new EngineException("Too many recursive calls");
			}
			bool stop=false;
            BaseVariable last=null;
			while(!stop){
                if (Tokens.Count <= pos)
                {
                    RecursionCount--;
                    return new ScriptState(pos, last);
                }
				var t=Tokens[pos];
				if(Keywords.Contains(t.Element)){
					if(t.Element=="var"){
						KeywordVar(ref pos,local);
					}else if(t.Element=="if"){
						var res=KeywordIf(ref pos, local);
						if(res!=null){
                            RecursionCount--;
                            return new ScriptState(pos, res);
						}
					}else if(t.Element=="return"){
						var tmp=KeywordReturn(ref pos, local);
						RecursionCount--;
                        return new ScriptState(pos, tmp);
					}else if(t.Element=="global"){
						pos++;
						if(Tokens[pos].Element!="var"){
							throw new SyntaxException("Expected `var` after global", pos);
						}
						KeywordVar(ref pos, Global);
					}else if(t.Element=="while"){
						var res=KeywordWhile(ref pos, local);
						if(res!=null){
                            RecursionCount--;
                            return new ScriptState(pos, res);
						}
					}else if(t.Element=="foreach"){
						var res=KeywordForeach(ref pos, local);
						if(res!=null){
                            RecursionCount--;
							return new ScriptState(pos, res);
						}
					}else{
						throw new SyntaxException("Unknown keyword", pos);
					}
				}else if(t.Element=="}"){
					if(CurlyCount==0){
						if(RecursionCount==0){ 
							throw new SyntaxException("Expected `return` not `}` to end the top-level program", pos);
						}
						RecursionCount--;
						return new ScriptState(pos, last);
					}else{
						DeleteScope(pos,local);
						CurlyCount--;
					}
				}else if(t.Element=="{"){
					CreateScope(pos,local);
					CurlyCount++;
				}else if(t.Element!=";"){
					last=EvaluateExpression(ref pos,local,new string[]{";"});
				}else{
					throw new SyntaxException("Unexpected symbol",pos); //should never reach here
				}
				
				
				pos++;
			}
            RecursionCount--;
            return new ScriptState(pos, last);
		}
		BaseVariable KeywordForeach(ref int pos, VariableList local)
		{
			pos++;
			if(Tokens[pos].Element!="("){
				throw new SyntaxException("Expecting ( after `foreach` keyword", pos);
			}
			pos++;
			if(Tokens[pos].Hint!=ElementHint.Identifier){
				throw new SyntaxException("Expecting variable name", pos);
			}
			string name=Tokens[pos].Element;
			VariableContainer val=FindVariable(name, local); //will implicitly add a variable
			VariableContainer key=null;
			pos++;
			if(Tokens[pos].Element=="="){
				pos++;
				if(Tokens[pos].Element!=">"){
					throw new SyntaxException("Expecting either `in` or `=>`", pos);
				}
				pos++;
				key=val;
				val=FindVariable(Tokens[pos].Element, local);
				pos++;
			}
			if(Tokens[pos].Element!="in"){
				throw new SyntaxException("Expecting `in`", pos);
			}
			pos++;
			var table=EvaluateExpression(ref pos, local, new string[]{")"});
			if(!(table is TableVariable)){
				throw new RuntimeException("Expecting table variable as foreach argument", pos);
			}
			pos++;
			if(Tokens[pos].Element!="{"){
				throw new NotImplementedException("Must encase statement in `{}`");
			}
			pos++;
			int beginpos=pos;
			var keys=(TableVariable)table.KeyList();
			if(key==null){
				key=new VariableContainer("");
			}
			var count=((IntegerVariable)table.KeyCount()).IntValue; //potentially expensive operation and could have bad consequences
			for(int i=0;i<count;i++){
				key.Var=keys.SubScriptGet(new IntegerVariable(i));
				val.Var=table.SubScriptGet(key.Var);
				CreateScope(pos, local);
				var res=Execute(pos, local);
				DeleteScope(pos, local);
				if(res!=null){
					DeleteScope(pos, local);
					return res.LastResult;
				}
			}
			DeleteScope(pos, local);
			pos+=BlockLength(pos);
			return null;
		}
		void CreateScope(int pos,VariableList local)
		{
			ScopeMarkerVariable v=new ScopeMarkerVariable("scope@" + pos.ToString());
			VariableContainer c=new VariableContainer("!scopemarker", v);
			local.Add(c);
		}
		void DeleteScope(int pos,VariableList local)
		{
			for (int i=local.Count-1;i>=0;i--)
			{
				if (local[i].Var is ScopeMarkerVariable)
				{
					local.RemoveRange(i,local.Count-i);
				}
			}
		}
		FunctionVariable ParseFunction(ref int pos, bool inline){
			if(inline){
				//function is in format of `function Foo(bar){}` rather than `var Foo=function(bar){};`
				
			}
			VariableList args=new VariableList();
			pos++;
			if(Tokens[pos].Element!="("){
				throw new SyntaxException("Expecting `(` after function keyword", pos);
			}
			pos++;
			while(Tokens[pos].Element!=")"){
				var t=Tokens[pos];
				if(t.Hint!=ElementHint.Identifier){
					throw new SyntaxException("Expecting a valid identifier as an argument name", pos);
				}
				if(Keywords.Contains(t.Element)){
					throw new SyntaxException("Argument names can not be reserved keywords", pos);
				}
				args.Add(new VariableContainer(t.Element));
				if(Tokens[pos+1].Element!="," && Tokens[pos+1].Element!=")"){
					throw new SyntaxException("Arguments should be seperated with `,` or ended with `)`", pos);
				}
				if(Tokens[pos+1].Element!=")"){
					pos+=2;
				}else{
					pos++;
				}
			}
			pos++;
			if(Tokens[pos].Element!="{"){
				throw new SyntaxException("Function body should begin with `{`", pos);
			}
			pos++;
			int start=pos;
			int end=pos+BlockLength(start);
			pos=end;
			return new FunctionVariable(start, args); //start+1 because we need it to be after the `{`
		}
		int BlockLength(int pos){
			int count=0;
			int depth=0;
			while(true)
			{
				if(pos+count>=Tokens.Count){
					throw new SyntaxException("Expected '}' to end block. Reached end-of-file", pos);
				}
				if(Tokens[pos+count].Element=="{"){
					depth++;
				}
				if(Tokens[pos+count].Element=="}"){
					if(depth>0){
						depth--;
					}else{
						return count;
					}
				}
				count++;
			}
		}
		public List<ExpressionToken> ParseExpression(ref int pos,VariableList local,string[] terminators)
		{
			bool stop=false;
			List<ExpressionToken> list=new List<ExpressionToken>();
			ExpressionToken token=new ExpressionToken();
			ExpressionToken last=null;
			bool lastop=false;
			int originalpos=pos;
			while(!stop)
			{
				int lastpos=pos;
				var element=Tokens[pos].Element;
				string peek=null;
				if(pos<Tokens.Count-1){
					peek=Tokens[pos+1].Element;
				}
				foreach(var t in terminators){
					if(Prepare.IsIdentifier(t[0]) || t.Length==1){
						if(t==Tokens[pos].Element){
							stop=true;
						}
					}else{ //add this bit in to support `=>` as a terminator
						if(t.Length>2){
							throw new NotSupportedException();
						}
						if(t[0].ToString()==Tokens[pos].Element){
							if(t[1].ToString()==Tokens[pos+1].Element){
								pos++;
								stop=true;
							}
						}
					}
				}
				if(stop){
					return list;
				}
				token.IsOperation=true; //assume
				if(Tokens[pos].Hint==ElementHint.Operation)
				{
					
					
					//lastop is only set if the optype is left-right. This allows `123 as string + "foo"` but disallows `123 + *213` or 
					//allows for parsing of otherwise ambiguous operations such as `123 + -(5)` 
					if(last!=null && !last.IsOperation){
						lastop=false;
					}else{
						lastop=true;
						if(last!=null && !(last.Op.OpType==OperationType.LeftRightOp)){
							lastop=false; //use to determine if unary or binary
						}
					}
					if(element=="("){
						if(lastop){
							//do sub expression
							pos++;
							token.IsOperation=false;
							token.Value=new ScriptValue(pos,this,
								                      new VariableContainer("",
								                      EvaluateExpression(ref pos,local,new string[]{")"})));
							
							pos++;
						}else{
							//function call
							var l=new VariableList();
							pos++;
							while(Tokens[pos].Element!=")"){
								var val=EvaluateExpression(ref pos, local, new string[]{",",")"});
								l.Add(new VariableContainer("argument@"+pos.ToString(),val));
								if(Tokens[pos].Element!=")"){
									pos++;
								}
							}
							pos++;
							token.Op=new FunctionCallOperation(l);
						}
					}else if(Tokens[pos].Element==")" && terminators.Contains(")")){ //don't care about `)` if it's what terminates the expression
						throw new SyntaxException("Unmatched closing parenthesis", pos);
					}else if(element=="+"){
						if(!lastop){
							if(peek=="+"){
								 //postfix ++
								token.Op=new PostIncrementOperation();
								pos++;
							}else{
								token.Op=new BinaryAddOperation();
							}
						}else{
							if(peek=="+"){
								//prefix ++
								token.Op=new PreIncrementOperation();
								pos++;
							}else{
								token.Op=new UnaryPlusOperation();
							}
						}
						pos++;
					}else if(element=="*"){
						if(lastop){
							throw new SyntaxException("no lvalue for * multiply operation", pos);
						}
						token.Op=new BinaryMultiplyOperation();
						pos++;
					}else if(element=="/"){
						if(lastop){
							throw new SyntaxException("no lvalue for / divide operation", pos);
						}
						token.Op=new BinaryDivideOperation();
						pos++;
					}else if(element=="%"){
						if(lastop){
							throw new SyntaxException("no lvalue for % modulo operation", pos);
						}
						token.Op=new BinaryModuloOperation();
					}else if(element=="-"){
						if(lastop){
							if(peek=="-"){
								//prefix --
								token.Op=new PreDecrementOperation();
								pos++;
							}else{
								//unary -
								token.Op=new UnaryMinusOperation();
							}
						}else{
							if(peek=="-"){
								//postfix --
								token.Op=new PostDecrementOperation();
								pos++;
							}else{
								//binary subtract
								token.Op=new BinarySubtractOperation();
							}
						}
						pos++;
					}else if(element=="="){
						if(peek=="="){
							//equivalence operator
							token.Op=new EqualOperation();
							pos++;
						}else{
							//assignment
							token.Op=new AssignmentOperation();
						}
						pos++;
					}else if(element=="<"){
						if(peek=="<"){
							pos++;
							token.Op=new ShiftLeftOperation();
						}else if(peek=="="){
							pos++;
							token.Op=new LessThanOrEqualOperation();
						}else{
							token.Op=new LessThanOperation();
						}
						pos++;
					}else if(element==">"){
						if(peek==">"){
							pos++;
							token.Op=new ShiftRightOperation();
						}else if(peek=="="){
							//greater than or equal
							pos++;
							token.Op=new GreaterThanOrEqualOperation();
						}else{
							token.Op=new GreaterThanOperation();
						}
						pos++;
					}else if(element=="["){
						if(lastop){
							//table declaration
							pos++;
							var t=ParseTable(ref pos, local);
							token.IsOperation=false;
							token.Value=new ScriptValue(pos, this, new VariableContainer("",t));
							pos++;
						}else{
							//subscript
							pos++;
							var key=EvaluateExpression(ref pos, local, new string[]{"]"});
							pos++;
							token.Op=new SubscriptOperation(key);
						}
					}else if(element=="."){
						//subscript syntactic sugar
						pos++;
						if(Tokens[pos].Hint!=ElementHint.Identifier){
							throw new SyntaxException("Expecting identifier after table subscript shortcut operator(`.`)", pos);
						}
						var key=new StringVariable(Tokens[pos].Element);
						token.Op=new SubscriptOperation(key);
						pos++;
					}else if(element=="&"){
						if(peek=="&"){
							throw new NotImplementedException("The `&&` logical AND operator is currently not supported");
						}
						token.Op=new BinaryAndOperation();
						pos++;
					}else if(element=="|"){
						if(peek=="|"){
							throw new NotImplementedException("The `||` logical OR operator is currently not supported");
						}
						token.Op=new BinaryOrOperation();
						pos++;
					}else if(element=="!"){
						if(peek=="="){
							pos++;
							token.Op=new NotEqualOperation();
						}else{
							token.Op=new LogicalNotOperation();
						}
						pos++;
					}else if(element=="~"){
						token.Op=new BitwiseNotOperation();
						pos++;
					}else{
						throw new SyntaxException("Unknown or not implemented operator/symbol", pos);
					}
					
				}else if(Tokens[pos].Element=="function"){
					token.IsOperation=false;
					token.Value=new ScriptValue(pos,this,new VariableContainer("",ParseFunction(ref pos, false)));
					pos++;
				}else if(Tokens[pos].Element=="as"){
					pos++;
					CoreTypes t=ParseType(pos,Tokens[pos].Element);
					token.Op=new CastOperation(t);
					pos++;
					
				}else if(Tokens[pos].Element=="is"){
					pos++;
					CoreTypes t=ParseType(pos,Tokens[pos].Element);
					token.Op=new IsTypeOperation(t);
					pos++;
				}else if(Tokens[pos].Element=="islike"){
					
				}else if(Tokens[pos].Element=="null" || Tokens[pos].Element=="nil"){
					token.IsOperation=false;
					token.Value=new ScriptValue(pos,this,new VariableContainer("", new NullVariable()));
					pos++;
				}else if(Tokens[pos].Element=="undefined"){
					token.IsOperation=false;
					token.Value=new ScriptValue(pos,this,new VariableContainer("", new UndefinedVariable()));
					pos++;
				}else if(Tokens[pos].Element=="true"){
					token.IsOperation=false;
					token.Value=new ScriptValue(pos,this,new VariableContainer("",new BooleanVariable(true)));
					pos++;
				}else if(Tokens[pos].Element=="false"){
					token.IsOperation=false;
					token.Value=new ScriptValue(pos,this,new VariableContainer("",new BooleanVariable(false)));
					pos++;
				}else if(Tokens[pos].Hint==ElementHint.BeginQuote){
					token.IsOperation=false;
					pos++;
					token.Value=new ScriptValue(pos,this,
					                  new VariableContainer("",new StringVariable(Tokens[pos].Element))); //preprocessor makes this super easy
					pos++;
					pos++; 
				}else if(Tokens[pos].Hint==ElementHint.Identifier ||
				         Tokens[pos].Hint==ElementHint.Number){
					
					token.IsOperation=false;
				//token.Operation=TokenOperation.None;
					if(Prepare.IsAlpha(element[0]) || element[0]=='_'){
						//variable
						var v=FindVariable(element,local);
						//regular variable
						token.Value=new ScriptValue(pos,this,v);
						pos++;
					}else if(Prepare.IsNumeric(element[0])){
						if(Tokens.Count-pos>2 && 
						   Tokens[pos+1].Element=="." && Tokens[pos+2].Hint==ElementHint.Number){
							//float
							double tmp;
							if(double.TryParse(
								element+Tokens[pos+1].Element+Tokens[pos+2].Element,out tmp)==false){
								throw new SyntaxException("expected valid float value",pos);
							}
							token.Value=new ScriptValue(pos,this,new VariableContainer("",new FloatVariable(tmp)));
							pos+=3;
						}else{
							//must be an integer
							int tmp;
							if(int.TryParse(element,out tmp)==false){
								throw new SyntaxException("expected valid integer value",pos);
							}
							token.Value=new ScriptValue(pos,this,new VariableContainer("",new IntegerVariable(tmp)));
							pos++;
						}
					}
                }
				else
                {
					throw new SyntaxException("shouldn't get here", pos);
				}
				if(pos==lastpos){ //otherwise this would be an infinite loop.. and I'm very tired of having to debug this crap. 
					throw new SyntaxException("Problem parsing expression into value/operation tokens",pos);
				}
				last=token;
				list.Add(token);
				token=new ExpressionToken();
				
			}
			
			return null;
		}
		CoreTypes ParseType(int pos,string t)
		{
			t=t.ToLower();
			if(t=="int" || t=="integer"){
				return CoreTypes.Integer;
			}else if(t=="bool" || t=="boolean"){
				return CoreTypes.Boolean;
			}else if(t=="float" || t=="double"){
				return CoreTypes.Float;
			}else if(t=="function"){
				return CoreTypes.Function;
			}else if(t=="table" || t=="array"){
				return CoreTypes.Table;
			}else if(t=="null" || t=="nil"){
				return CoreTypes.Null;
			}else if(t=="string"){
				return CoreTypes.String;
			}else if(t=="undefined"){
				return CoreTypes.Undefined;
			}else{
				throw new SyntaxException("Type name is not valid", pos);
			}
		}
		Associativity[] OrderMap={ //basically a straight rip from C++'s order of operations/associativity map. We won't use all of these though
			Associativity.LeftToRight, //0 not used
			Associativity.LeftToRight, //1
			Associativity.LeftToRight, //2
			Associativity.RightToLeft, //3
			Associativity.LeftToRight, //4
			Associativity.LeftToRight, //5
			Associativity.LeftToRight, //6
			Associativity.LeftToRight, //7
			Associativity.LeftToRight, //8
			Associativity.LeftToRight, //9
			Associativity.LeftToRight, //10
			Associativity.LeftToRight, //11
			Associativity.LeftToRight, //12
			Associativity.LeftToRight, //13
			Associativity.LeftToRight, //14
			Associativity.RightToLeft, //15
			Associativity.RightToLeft, //16
			Associativity.LeftToRight, //17
			Associativity.LeftToRight //18
		};
		TableVariable ParseTable(ref int pos, VariableList local)
		{
			var table=new TableVariable();
			if(Tokens[pos].Element=="]"){
				return table;
			}
			int i=0;
			/*valid forms:
			 * [a, b, c]
			 * [key => value, key2 => value2]
			 * [key => value, value2]
			 * In last case, value2 has a key of `0` 
			 */
			pos--;
			while(Tokens[pos].Element!="]"){
				pos++;
				var t=EvaluateExpression(ref pos, local, new string[]{"=>", "]", ","});
				if(Tokens[pos].Element=="]" || Tokens[pos].Element==","){
					table.SubScriptSet(new IntegerVariable(i), t);
					i++;
				}else{ //=>
					pos++; 
					var val=EvaluateExpression(ref pos, local, new string[]{"]", ","});
					table.SubScriptSet(t, val);
				}
			}
			//shouldn't reach here
			return table;
			
		}
		public BaseVariable EvaluateExpression(ref int pos,VariableList local,string[] terminators)
		{
			var list=ParseExpression(ref pos, local, terminators);
			var exp=new ExpressionEvaluator(OrderMap.ToList());
			var tmp=(ScriptValue)exp.Evaluate(list);
			return tmp.Value.Var;
		}
		
		protected VariableContainer FindVariable(string name, VariableList local)
		{
			int tmp;
			tmp = local.Find(name);
			if (tmp != -1)
			{
				return local[tmp];
			}
			tmp = Global.Find(name);
			if (tmp == -1)
			{
				
				var vc= new VariableContainer(name,new UndefinedVariable());
				local.Add(vc); //should it be like this? 
				return vc; 
				//throw new RuntimeException("Could not find variable "+name,0);
			}
			return Global[tmp];
		}
    }
	public class ScriptValue : ExpressionValue
	{
		public ScriptValue(){}
		public ScriptValue(int pos,ScriptingEngine s,VariableContainer v)
		{
			Position=pos;
			Script=s;
			Value=v;
		}
		public int Position{get;set;}
		public ScriptingEngine Script{get;set;}
		public VariableContainer Value{get;set;}
		public TableVariable Table{get;set;}
		public BaseVariable Key{get;set;}
	}
		
}
