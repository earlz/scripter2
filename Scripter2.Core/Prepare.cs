/*
<Copyright Header>
Copyright (c) 2012 Jordan "Earlz/hckr83" Earls  <http://lastyearswishes.com>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.
   
THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL
THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

</Copyright Header>
*/
using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;

namespace Earlz.Scripter2.Core
{
	//First pass "preprocessor". Will take the raw script as a string and parse it into machine parsable chunks
	//Also provides hints as to what each chunk is to make it easier later
	//A C# port from BSD licensed JFBuild written in C (second time I've done this. grrr)
	public class SyntaxElement
	{
		public SyntaxElement(string e, int l, int pos,ElementHint h)
		{
			Element = e;
			Line = l;
			RawPosition = pos;
			Hint=h;
		}
		public ElementHint Hint;
		public string Element;
		public int RawPosition; //Raw position in the string buffer
		public int Line;	
	}
	public enum ElementHint
	{
		Unknown, //really, this should never be exposed.. but just in case
		BeginQuote,
		EndQuote,
		Quoted,
		Number,
		Identifier,
		Operation
	}
	
	public class Prepare
	{
		int pos; //have it global to keep things easy
		StringBuilder Target;
		static readonly char[] TemplateQuoteChars=new char[]{'\'', '\"'};
		static char[] QuoteChars;
		int CurrentLine=1;
		public List<SyntaxElement> Tokens;
		ElementHint CurrentHint;
		public Prepare(string contents)
		{
			QuoteChars=TemplateQuoteChars;
			pos = 0;
			Process(contents);
		}
		public Prepare(string contents,char[] quote_chars)
		{
			QuoteChars = quote_chars;
			pos = 0;
			Process(contents);
		}


		//helper functions
		public static bool IsAlpha(char c)
		{
			return ((c >= 'A') && (c <= 'Z')) || ((c >= 'a') && (c <= 'z'));
		}
		public static bool IsNumeric(char c)
		{
			return (c >= '0') && (c <= '9');
		}
		public static  bool IsWhitespace(char c)
		{
			return (c == ' ') || (c == '\t') || (c == '\n') || (c == '\r');
		}
		public static bool IsQuote(char c)
		{
			return QuoteChars.Contains(c);
		}
		public static bool IsIdentifier(char c)
		{
			return IsAlpha(c) || IsNumeric(c) || (c == '_'); //include underscore
		}

		protected void AddToTarget(char c)
		{
			Target.Insert(Target.Length, new char[]{c});
		}
		private void Process(string source)
		{
			Target = new StringBuilder();
			Tokens = new List<SyntaxElement>();
			char last;
			char current;
			last = ' '; //spaces are a good neutral character
			while (pos < source.Length)
			{
				current = source[pos];
				//check for comments
				if (current == '/')
				{ 
					char tmp = source[pos + 1];
					if (tmp == '*' || tmp == '/')
					{
						StripComment(tmp, source);
					}
					else
					{
						//otherwise, is not a comment
						if (IsIdentifier(last))
						{
							Tokens.Add(new SyntaxElement(Target.ToString(), CurrentLine, pos,CurrentHint));
						}
						Target = new StringBuilder();
						AddToTarget(current);
						Tokens.Add(new SyntaxElement(Target.ToString(), CurrentLine, pos+(IsIdentifier(last)?0:1),ElementHint.Operation));
						CurrentHint=ElementHint.Unknown;
						Target = new StringBuilder();
					}
				}
				else if (IsQuote(current))
				{
					//quotes
					ProcessQuotes(source,current);
				}
				else if (IsIdentifier(current))
				{ 
					//identifiers and numbers
					if(Target.Length==0){
						if(IsNumeric(current)){
							CurrentHint=ElementHint.Number;
						}else{
							CurrentHint=ElementHint.Identifier;
						}
					}
					AddToTarget(current);
					last = current;
				}
				else if (IsWhitespace(current))
				{
					//whitespace (only handled if it's significant)
					CheckLineBreak(source);
					if (IsIdentifier(last))
					{ 
						//if whitespace after identifier begin new string.
						last = ' ';
						Tokens.Add(new SyntaxElement(Target.ToString(), CurrentLine, pos,CurrentHint));
						CurrentHint=ElementHint.Unknown;
						Target = new StringBuilder();

					}
					//ignore whitespace
				}
				else
				{
					//special symbol
					if (IsIdentifier(last))
					{
						Tokens.Add(new SyntaxElement(Target.ToString(), CurrentLine, pos, CurrentHint));
						CurrentHint=ElementHint.Unknown;

					}
					Target = new StringBuilder();
					AddToTarget(current);
					Tokens.Add(new SyntaxElement(Target.ToString(), CurrentLine, pos+(IsIdentifier(last)?0:1),ElementHint.Operation));
					CurrentHint=ElementHint.Unknown;
					Target = new StringBuilder();
				}
				last = current;
				pos++;
			}
		}

		public SyntaxElement ResolvePosition(int at)
		{
			pos=0;
			while (pos < Tokens.Count)
			{
				if (at == Tokens[pos].RawPosition)
				{
					return Tokens[pos];
				}
				if (at < Tokens[pos].RawPosition)
				{
					//Went too far. return token before it
					if (pos > 0)
					{
						return Tokens[pos - 1];
					}
				}
				pos++;
			}
			return Tokens[Tokens.Count-1]; //didn't find it, so just return last token
		}

		private void StripComment(char c, string source)
		{
			if (c == '/')
			{ 
				//single line comment -- '//'
				CheckLineBreak(source);
				while (source[pos] != '\n')
				{
					if (pos >= source.Length)
					{
						break;
					}
					pos++;
					if (pos >= source.Length)
					{
						break;
					}
					CheckLineBreak(source);
				}
			}
			else if (c == '*')
			{
				//multi-line
				while (pos < source.Length && source[pos]!='*' || source[++pos]!='/') //notice the careful use of shortcuts
				{
					pos++;
				}
				if(pos>=source.Length)
				{
					throw new PreprocessingException("Multi-line comment started but never ended. Expecting `*/` but found EOF", CurrentLine);
				}
			}
			else
			{ 
				//not an error, they just had a '/' in their script
				return;
			}
		}

		void CheckLineBreak(string source)
		{
			if (source[pos] == '\n')
			{
				CurrentLine++;
			}
			if (source[pos] == '\r')
			{
				if (source[pos + 1] == '\n')
				{
					pos++;
				}
				CurrentLine++;
			}

		}

		private void ProcessQuotes(string source,char quote_char)
		{
            int beginline = CurrentLine;
			Tokens.Add(new SyntaxElement(source[pos].ToString(), CurrentLine, pos,ElementHint.BeginQuote));
			pos++;
			Target = new StringBuilder();
			while (true)
			{
				if (pos >= source.Length)
				{
					throw new PreprocessingException(string.Format("Found EOF. Expecting ending `{0}` for matching beginning quote at line {1}", quote_char.ToString(), beginline), CurrentLine);
				}
				if (source[pos]==quote_char) 
				{
					Tokens.Add(new SyntaxElement(Target.ToString(), CurrentLine, pos, ElementHint.Quoted));
					Tokens.Add(new SyntaxElement(source[pos].ToString(), CurrentLine, pos+1, ElementHint.EndQuote));
					Target = new StringBuilder();
					return;
				}
				if (source[pos] == '\\')
				{
					pos++;
					switch (source[pos])
					{
						case 'n':
							AddToTarget('\n');
							break;
						case 'r':
							AddToTarget('\r');
							break;
						case '\"':
							AddToTarget('\"');
							break;
						case '\\':
							AddToTarget('\\');
							break;
						case '\'':
							AddToTarget('\'');
							break;
						//todo add generic quote
						default:
							throw new PreprocessingException("Unknown escape character", CurrentLine);;
					}

				}
				else
				{
					CheckLineBreak(source);
					AddToTarget(source[pos]);
					
				}

				pos++;
			}
		}

	}
}

