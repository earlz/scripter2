﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Earlz.Scripter2.Core
{
    public class ScriptState
    {
        public ScriptState(int pos, BaseVariable last)
        {
            Position = pos;
            LastResult = last;
        }
        public int Position { get; set; }
        public BaseVariable LastResult { get; set; }
    }
}
