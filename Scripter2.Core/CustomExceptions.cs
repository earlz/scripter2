/*
<Copyright Header>
Copyright (c) 2012 Jordan "Earlz/hckr83" Earls  <http://lastyearswishes.com>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.
   
THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL
THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

</Copyright Header>
*/
using System;

namespace Earlz.Scripter2.Core
{
	public class EngineException : Exception
	{
		public int Position;
		public string Error;
		public EngineException(string msg): base(msg){
			Error=msg;
			Position=-1;
		}
		public EngineException(string msg, int pos) : base(msg){
			Error=msg;
			Position=pos;
		}
	}
	public class PreprocessingException : EngineException
	{
		public int LineNumber;
		public PreprocessingException(string msg, int line): base(msg){
			LineNumber=line;
		}
	}
	public class SyntaxException : EngineException
	{
		public SyntaxException(string error,int pos) : base(error, pos){}
	}
	public class RuntimeException : EngineException
	{
		public RuntimeException(string error,int pos) : base(error, pos){
		}
	}
	public class IncompatibleCastException : EngineException
	{
		public IncompatibleCastException(BaseVariable val, CoreTypes cast):base("Incompatible type cast"){
			Error="Error casting value of type "+val.CoreType.ToString()+" to type "+cast.ToString();
		}
	}
	public class TimeoutException : EngineException
	{
		public TimeoutException() : base("Script Timeout") {
			Error="Script Timeout";
		}
	}

	public class OperatorNotSupportedException : Exception
	{
		public OperatorNotSupportedException(VariableOperator op,BaseVariable arg1, BaseVariable arg2)
		{
			
		}
	}
    public class TypeConversionException : Exception
    {
        public TypeConversionException(BaseVariable arg1, BaseVariable arg2)
        {

        }
    }
    public class VariableNotFoundException : Exception
    {
        public VariableNotFoundException(string msg)
            : base(msg)
        {
        }
    }
}

