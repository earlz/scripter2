﻿using Earlz.Scripter2.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scripter2.Interactive
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Scripter2 Interactive Mode");
            var engine = new ScriptingEngine();
            while (true)
            {
                Console.Write("> ");
                string line=Console.ReadLine();
                Console.WriteLine(engine.ExecuteChunk(line)); //easy enough heh
            }
        }
    }
}
